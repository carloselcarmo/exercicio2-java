package br.com.Itau;

import java.util.Random;

public class Dado
{
    private int numeroFaces;

    public Dado(int numeroFaces)
    {
        this.numeroFaces = numeroFaces;
    }

    public Integer Jogar()
    {
        Random random = new Random();
        return random.nextInt(this.numeroFaces) + 1;
    }
}

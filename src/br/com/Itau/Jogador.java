package br.com.Itau;

public class Jogador
{
    private String nome;
    private Dado dado;

    public String getNome()
    {
        return nome;
    }

    public Jogador (String Nome)
    {
        this.nome = Nome;
        dado = new Dado(6);
    }

    public void trocarDado(Integer quantidadeFaces)
    {
        dado = new Dado(quantidadeFaces);
    }

    public Integer sortear()
    {
        Integer resultadoDado = dado.Jogar();
        return resultadoDado;
    }

    public void jogarDadosComResultado(int quantidadeDeDados)
    {
        Resultado resultado = new Resultado();
        for(int i=0; i<quantidadeDeDados;i++)
        {
            Integer valorSorteado = sortear();
            resultado.adicionarValor(valorSorteado);
        }
        IO.imprimir(resultado);
        IO.pularLinha();
    }

    public void jogarMelhorDeXVezes(int quantidadeDeVezes, int quantidadeDeDados)
    {
        for(int i=0; i<quantidadeDeVezes;i++)
        {
            jogarDadosComResultado(quantidadeDeDados);
        }
    }
}

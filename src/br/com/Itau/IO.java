package br.com.Itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO
{
    public static void imprimirInicioExercicio (int numero)
    {
        System.out.println("****Exercicio " + numero + "            *******");
    }

    public static void imprimirFimExercicio ()
    {
        System.out.println("**********************************");
    }

    public static Map<String, Integer> solicitarQuantidadeGrupos()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe a Quantidade de Grupos: ");
        int quantidadeGrupos = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("quantidadeGrupos", quantidadeGrupos);

        return dados;
    }

    public static Map<String, Integer> solicitarQuantidadeDadosJogados()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe a Quantidade de Dados a serem jogados: ");
        int quantidadeDados = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("quantidadeDados", quantidadeDados);

        return dados;
    }

    public static Map<String, Integer> solicitarQuantidadeDeFacesDoDado()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Informe a Quantidade de Faces do Dado: ");
        int quantidadeFaces = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("quantidadeFaces", quantidadeFaces);

        return dados;
    }

    public static void imprimir(int valor)
    {
        System.out.print(valor);
    }

    public static void imprimir(Resultado resultado)
    {
        for (Map.Entry<Integer,Integer> valor: resultado.getValores().entrySet())
        {
            imprimir(valor.getValue());
            System.out.print(", ");
        }

        imprimir(resultado.calcularSoma());
    }

    public static void pularLinha()
    {
        System.out.println("");
    }
}

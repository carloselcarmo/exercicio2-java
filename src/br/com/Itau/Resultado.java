package br.com.Itau;

import java.util.HashMap;
import java.util.Map;

public class Resultado
{
    HashMap<Integer, Integer> Valores;

    public HashMap<Integer, Integer> getValores()
    {
        return Valores;
    }

    public Resultado()
    {
        Valores = new HashMap<>();
    }

    public void adicionarValor(Integer valor)
    {
        Valores.put(Valores.size() + 1, valor);
    }

    public Integer calcularSoma()
    {
        Integer resultado = 0;
        for (Map.Entry<Integer,Integer> valor: Valores.entrySet())
        {
                resultado += valor.getValue();
        }

        return resultado;
    }
}

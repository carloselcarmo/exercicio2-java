package br.com.Itau;

import java.util.Map;

public class Main
{

    public static void main(String[] args)
    {
        Jogador jogador = new Jogador("Zé Mané");

        Map<String, Integer> dadosQtdDados = IO.solicitarQuantidadeDadosJogados();
        Map<String, Integer> dadosQtdGrupos = IO.solicitarQuantidadeGrupos();
        Map<String, Integer> dadosQtdFaces = IO.solicitarQuantidadeDeFacesDoDado();

        jogador.trocarDado(dadosQtdFaces.get("quantidadeFaces"));

        IO.imprimirInicioExercicio(1);
        IO.imprimir(jogador.sortear());
        IO.pularLinha();
        IO.imprimirFimExercicio ();

        IO.imprimirInicioExercicio(2);
        jogador.jogarDadosComResultado(dadosQtdDados.get("quantidadeDados"));
        IO.imprimirFimExercicio ();

        IO.imprimirInicioExercicio(3);
        jogador.jogarMelhorDeXVezes(dadosQtdGrupos.get("quantidadeGrupos"), dadosQtdDados.get("quantidadeDados"));
        IO.imprimirFimExercicio ();
    }
}
